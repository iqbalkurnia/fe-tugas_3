-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Bulan Mei 2022 pada 18.35
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm`
--

--
-- Dumping data untuk tabel `departemen`
--

INSERT INTO `departemen` (`id`, `nama`) VALUES
(1, 'Manajemen'),
(2, 'Pengembangan Bisnis'),
(3, 'Teknisi'),
(4, 'Analis');

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES
(1, 'Rizki Saputra', 'L', 'Menikah', '10/11/1980', '1/1/2011', 1),
(2, 'Farhan reza', 'L', 'Menikah', '11/1/1989', '1/1/2011', 1),
(3, 'Riyando Adi', 'L', 'Menikah', '1/25/1977', '1/1/2021', 1),
(5, 'Satya Laksana', 'l', 'Menikah', '1/12/1981', '3/19/2011', 2),
(6, 'Miguel Hernandez', 'L', 'Menikah', '10/16/1994', '6/15/2014', 2),
(7, 'Putri Persada', 'P', 'Menikah', '1/30/1988', '4/14/2013', 2),
(9, 'Haqi Hafiz', 'L', 'Menikah', '9/19/1995', '3/9/2015', 3),
(10, 'Abi Isyawara', 'L', 'Menikah', '6/3/1991', '1/22/2012', 3),
(12, 'Nadia Aulia', 'P', 'Menikah', '10/7/1989', '5/7/2012', 4),
(13, 'Mutriara Rezki', 'P', 'Menikah', '3/23/1988', '5/21/2013', 4),
(14, 'Dani Setiawan', 'L', 'Menikah', '2/11/1986', '11/30/2014', 4),
(15, 'Budi Putra', 'L', 'Belum', '10/23/1995', '12/3/2015', 4);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
